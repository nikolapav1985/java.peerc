import javax.swing.JOptionPane;

/**
* Quiz class
*
* A quiz example. One reply is correct and makes quiz attempt successful.
* All other replies (single letter) are incorrect (these cause a quiz to continue).
* All other replies having length more than one letter are incorrect and invalid.
*
* compile example ----- javac Quiz.java
*
* run example ------ java Quiz
*
*/
class Quiz{
    public static int nQuestions = 3; // total number of questions
    public static int nCorrect = 0; // number of correct attempts
    public static String ask(String question){ // ask question and check valid reply
        String valid="ABCDE";
        String reply="";
        for(;1==1;){ // keep looping until reply valid
            reply = JOptionPane.showInputDialog(question);
            reply = reply.toUpperCase();
            if(reply.length() == 1 && valid.contains(reply)){ // reply valid, break loop
                break;
            } else { // reply not valid, keep loop
                JOptionPane.showMessageDialog(null, "Please try again. Reply is not valid!");
            }
        }
        return reply;
    }
    public static void check(String question, String correctReply){ // check if reply correct
        String reply="";
        reply=ask(question);
        if(reply.equals(correctReply)){
            JOptionPane.showMessageDialog(null, "Correct!");
            nCorrect++;
        }else{
            JOptionPane.showMessageDialog(null, "Incorrect. The correct reply is "+correctReply+"!");
        }
    }
    public static void main(String[] args){
        String question = "Best method for data input?\n";
        String questionb = "Most popular drink in the morning?\n";
        String questionc = "Best code editor?\n";
        String[] questions = {"A","B","C"};
        String reply = "";
        String correct = "A";
        String correctb = "A";
        String correctc = "C";
        String[] corrects = {"A","B","C"};
        question += "A keyboard, \n";
        question += "B mouse, \n";
        question += "C gesture, \n";
        question += "D voice, \n";
        question += "E stylus";
        questionb += "A coffee, \n";
        questionb += "B tea, \n";
        questionb += "C milk, \n";
        questionb += "D orange juice, \n";
        questionb += "E apple juice";
        questionc += "A notepad, \n";
        questionc += "B notepad++, \n";
        questionc += "C emacs, \n";
        questionc += "D vim, \n";
        questionc += "E visual studio";
        questions[0] = question;
        questions[1] = questionb;
        questions[2] = questionc;
        corrects[0] = correct;
        corrects[1] = correctb;
        corrects[2] = correctc;
        int i=0;

        for(;i<nQuestions;i++){ // ask questions
            check(questions[i],corrects[i]);
        }

        JOptionPane.showMessageDialog(null, "The score is "+nCorrect+" out of "+nQuestions+"!");
    }
}
